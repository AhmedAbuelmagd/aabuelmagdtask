# README #


### API Endpoints ###

- Base URL: "https://jsonplaceholder.typicode.com/" 
- User: "\users" (GET)
- you can choose any user to start with Albums: "\albums" (userId as a parameter) (GET)
- Photos: "\photos" (albumId as a parameter) (GET)
### Design pattern ###
- MVVM with Closure
### Task screens ###
- Profile screen -> ProfileVC
- Photos screen -> PhotosVC
- ImageViewer screen -> ImageViewerVC

### Profile screen ###
- preview user data and list of albume 
- no storyboard 
### Photos screen ###
- preview album images
- there is search bar
- no storyboard 
### ImageViewer screen ###
- preview one image with zooming and sharing functionality