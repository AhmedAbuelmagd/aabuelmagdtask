//
//  NetworkManager.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import Foundation
import Moya

// MARK: - Error
enum GetApiFailureReason: Int, Error {
    case unAuthorized = 401
    case badRequest = 400
    case notFound = 404
    case apiError = 500
    case notHandleStatusCode = 0
}

extension GetApiFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .unAuthorized: return ERRORS.UNAUTHORIZED.rawValue
        case .notFound: return ERRORS.UNKNOWN.rawValue
        case .badRequest: return ERRORS.BAD_REQUEST.rawValue
        case .apiError: return ERRORS.UNKNOWN.rawValue
        case .notHandleStatusCode: return ERRORS.UNKNOWN.rawValue
        }
    }
}
protocol NetworkProtocol {
    func getUser(completionHandler: @escaping (Result<User, Error>)->())
    func getAlbums(userId: Int, completionHandler: @escaping (Result<[Album], Error>)->())
    func getPhotos(albumId: Int, completionHandler: @escaping (Result<[Photo], Error>)->())

}

class NetworkManager {
    let provider = MoyaProvider<APIs>(plugins: [NetworkLoggerPlugin()])
    
    private func fetchData<M: Decodable>(target: APIs, responseClass: M.Type, completionHandler: @escaping (Result<M, Error>)->()) {
        provider.request(target) { result in
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    do {
                        let model = try JSONDecoder().decode(M.self, from: response.data)
                        completionHandler(.success(model))
                    }catch let error{
                        completionHandler(.failure(error))
                    }
                }else {
                    if let reason = GetApiFailureReason(rawValue: response.statusCode) {
                         completionHandler(.failure(reason))
                    }else {
                        completionHandler(.failure(GetApiFailureReason.notHandleStatusCode))
                    }
                }

            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}


extension NetworkManager: NetworkProtocol {
    func getPhotos(albumId: Int, completionHandler: @escaping (Result<[Photo], Error>) -> ()) {
        fetchData(target: .getPhotos(albumId: albumId), responseClass: [Photo].self) { result in
                    completionHandler(result)
        }
    }
    
    func getAlbums(userId: Int, completionHandler: @escaping (Result<[Album], Error>) -> ()) {
        fetchData(target: .getAlbums(userId: userId), responseClass: [Album].self) { result in
                    completionHandler(result)
        }
    }
    
    func getUser(completionHandler: @escaping (Result<User, Error>) -> ()) {
        fetchData(target: .getUser, responseClass: User.self) { result in
                    completionHandler(result)
        }
    }
}

