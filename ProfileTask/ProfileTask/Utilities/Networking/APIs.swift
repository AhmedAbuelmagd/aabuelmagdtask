//
//  APIs.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import Foundation
import Moya


enum GeneralAppEndPoints: String {
    case USERS = "users/1"
    case ALBUMS = "albums"
    case PHOTOS = "photos"
}

enum APIs {
    case getUser
    case getAlbums (userId: Int)
    case getPhotos (albumId: Int)
}

extension APIs: TargetType {
    var baseURL: URL {
        guard let url = URL(string: Config.BASE_URL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    var path: String {
        switch self {
        case .getUser: return GeneralAppEndPoints.USERS.rawValue
        case .getAlbums: return GeneralAppEndPoints.ALBUMS.rawValue
        case .getPhotos: return GeneralAppEndPoints.PHOTOS.rawValue
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUser: return .get
        case .getAlbums: return .get
        case .getPhotos: return .get
        }

    }
    
    var task: Task {
        switch self {
        case .getUser: return .requestPlain
        case .getAlbums(let userId):
            return .requestParameters(parameters: ["userId": userId] , encoding: URLEncoding.queryString)
        case .getPhotos(let albumId):
            return .requestParameters(parameters: ["albumId": albumId] , encoding: URLEncoding.queryString)
        }
    }

    var sampleData: Data {
        return Data()
    }
        
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
