//
//  Enums.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 27/03/2021.
//

import Foundation

//MARK: - CELL IDENTIFIEERS Enum
enum CELL_IDENTIFIEERS: String, CaseIterable {
    case PROFILE = "cellId"
    case PHOTOS_HEADER = "PhotosCVHeaderCell"
    case LOADER = "LoaderView"
}

//MARK: - PLACEHOLDERS Enum
enum PLACEHOLDERS: String, CaseIterable {
    case PHOTO = "placeholder"
}

//MARK: - PLACEHOLDERS Enum
enum TITLES: String, CaseIterable {
    case MY_ALBUMS = "My Albums"
    case PROFILE = "Profile"
}
//MARK: - PLACEHOLDERS Enum
enum ERRORS: String, CaseIterable {
    case UNKNOWN = "unknownError"
    case UNAUTHORIZED = "Unauthorized"
    case BAD_REQUEST = "badRequst"
    case CELL_ERROR = "Error in cell"
}
// MARK: - Storyboard Enum
enum STORYBOARDS: String, CaseIterable {
    case MAIN = "Main"
}
// MARK: - Identifier Enum
enum IDENTIDIERS: String, CaseIterable {
    // # Main Storyboard
    case IMAGE_VIEWER = "ImageViewerVC"
}
