//
//  BaseVM.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import Foundation


protocol BaseVMProtocol {
    var showLoading: Bindable<Bool> { get set }
    var onShowError: ((_ alert: String) -> Void)?  { get set }
}
