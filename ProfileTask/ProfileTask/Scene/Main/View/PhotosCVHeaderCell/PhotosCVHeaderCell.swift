//
//  PhotosCVHeaderCell.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import UIKit

//MARK: - SearchDeleget Protocol
protocol SearchDeleget: NSObjectProtocol {
    func search(data: String)
}
class PhotosCVHeaderCell: UICollectionReusableView {
    
    // MARK: - Variables & Constants
    weak var searchDelegate: SearchDeleget?
    
    // MARK: - Outlets
    @IBOutlet weak var search: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.search.delegate = self
        addSeparatorLineToTop()
    }
    
    func addSeparatorLineToTop(){
        let line = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 0.5))
        line.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        addSubview(line)
    }
}
//MARK: - UISearchBarDelegate Extention
extension PhotosCVHeaderCell: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchDelegate?.search(data: searchBar.text ?? "")
    }
}
