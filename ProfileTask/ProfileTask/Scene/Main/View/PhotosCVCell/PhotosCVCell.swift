//
//  PhotosCVCell.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import UIKit
import Kingfisher

class PhotosCVCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var photoImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Init Cell Function
    func initCell(cellData: Photo){
        if let url = URL(string: (cellData.url?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")) {
            photoImgView.kf.indicatorType = .activity
            photoImgView.kf.setImage(with: url, placeholder: UIImage(named: PLACEHOLDERS.PHOTO.rawValue), options: [.transition(.fade(1))])
                }
    }
}
