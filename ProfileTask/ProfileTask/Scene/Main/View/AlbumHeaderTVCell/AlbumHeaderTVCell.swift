//
//  AlbumHeaderTVCell.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import UIKit

class AlbumHeaderTVCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var myAlbumLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLbl.font = UIFont.preferredFont(forTextStyle: .headline)
        myAlbumLbl.font = UIFont.preferredFont(forTextStyle: .headline)
        addressLbl.font = UIFont.preferredFont(forTextStyle: .callout)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func initCell(cellData: User){
        titleLbl.text = cellData.name
        addressLbl.text = "\(cellData.address?.street ?? ""), \(cellData.address?.city ?? ""), \(cellData.address?.suite ?? "") \(cellData.address?.zipcode ?? "")"
        myAlbumLbl.text = TITLES.MY_ALBUMS.rawValue
    }
    
}
