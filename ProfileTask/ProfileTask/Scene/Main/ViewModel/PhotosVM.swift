//
//  PhotosVM.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import Foundation

protocol PhotosVMProtocol {
    var albumId: Int { get set }
    var photosData: Bindable<[Photo]?> { get set }
    func getPhotos()
}

class PhotosVM: BaseVMProtocol, PhotosVMProtocol {
    
    var albumId: Int = 0
    var photosData = Bindable<[Photo]?>([])
    var showLoading: Bindable = Bindable(false)
    var onShowError: ((String) -> Void)?
    
    let apiClient: NetworkManager
    init(apiClient: NetworkManager = NetworkManager()) {
        self.apiClient = apiClient
    }
    func getPhotos() {
        showLoading.value = true
        apiClient.getPhotos(albumId: albumId){ [weak self] result in
            self?.showLoading.value = false
            switch result {
            case .success(let photosList):
                print("Response => \(photosList) <=")
                self?.photosData.value = photosList
            case .failure(let error):
                print("Error => \(error) <=")
                if let errors = error as? GetApiFailureReason {
                    self?.onShowError?(errors.getErrorMessage() ?? ERRORS.UNKNOWN.rawValue)
                }else {
                    self?.onShowError?(ERRORS.UNKNOWN.rawValue)
                }
            }
        }
    }
    func filterImages(searchData: String){
        let newData = photosData.value?.filter({$0.title!.lowercased().contains(searchData.lowercased())})
        self.photosData.value?.removeAll()
        self.photosData.value = newData
    }
}
