//
//  ProfileVM.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//
import Foundation

protocol ProfileVMProtocol {
    var userData: Bindable<User?> { get set }
    var albumsData: Bindable<[Album]?> { get set }
    func getUser()
    func getAlbums()
}

class ProfileVM: BaseVMProtocol, ProfileVMProtocol {
    var userData = Bindable<User?>(nil)
    var albumsData = Bindable<[Album]?>([])
    var showLoading: Bindable = Bindable(false)
    var onShowError: ((String) -> Void)?
    
    let apiClient: NetworkManager
    init(apiClient: NetworkManager = NetworkManager()) {
        self.apiClient = apiClient
    }
    func getUser() {
        apiClient.getUser { [weak self] result in
            switch result {
            case .success(let user):
                print("Response => \(user) <=")
                self?.userData.value = user
                self?.getAlbums()
            case .failure(let error):
                print("Error => \(error) <=")
                if let errors = error as? GetApiFailureReason {
                    self?.onShowError?(errors.getErrorMessage() ?? ERRORS.UNKNOWN.rawValue)
                }else {
                    self?.onShowError?(ERRORS.UNKNOWN.rawValue)
                }
                
            }
        }
    }
    func getAlbums() {
        showLoading.value = true
        apiClient.getAlbums(userId: userData.value?.id ?? 0){ [weak self] result in
            self?.showLoading.value = false
            switch result {
            case .success(let albumsList):
                print("Response => \(albumsList) <=")
                self?.albumsData.value = albumsList
            case .failure(let error):
                print("Error => \(error) <=")
                if let errors = error as? GetApiFailureReason {
                    self?.onShowError?(errors.getErrorMessage() ?? ERRORS.UNKNOWN.rawValue)
                }else {
                    self?.onShowError?(ERRORS.UNKNOWN.rawValue)
                }
            }
        }
    }
}

