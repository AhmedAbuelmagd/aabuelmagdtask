//
//  ProfileVC.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import UIKit

class ProfileVC: UIViewController {
    
    //MARK: - Variables & Constants
    var tableView = UITableView()
    let viewModel = ProfileVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        viewModel.getUser()
        view.backgroundColor = UIColor.white
        tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.backgroundColor = UIColor.white
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        navigationController?.navigationBar.prefersLargeTitles = true
        initTV(tableView)
    }
    
    func initTV(_ tv:UITableView){
        tv.delegate = self
        tv.dataSource = self
        tv.register(UITableViewCell.self, forCellReuseIdentifier: CELL_IDENTIFIEERS.PROFILE.rawValue)
        tv.tableFooterView = UIView()
        tv.registerNib(cell: AlbumHeaderTVCell.self)
        tv.sectionHeaderHeight = UITableView.automaticDimension
        tv.estimatedSectionHeaderHeight = 25
        tv.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        view = tv
    }
}

extension ProfileVC:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeue() as AlbumHeaderTVCell
        if let data = viewModel.userData.value{
            cell.initCell(cellData: data)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.albumsData.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIEERS.PROFILE.rawValue, for: indexPath) as UITableViewCell
            cell.selectionStyle = .none
            cell.textLabel?.text = viewModel.albumsData.value?[indexPath.row].title ?? ""
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PhotosVC()
        vc.albumTitle = viewModel.albumsData.value?[indexPath.row].title ?? ""
        vc.albumId = viewModel.albumsData.value?[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension ProfileVC{
    /// bind view model
    func bindViewModel(){
        // bind loading
        viewModel.showLoading.bind { [weak self] visible in
            if self != nil {
                visible ? showLoaderForController(getCurrentVC() ?? UIViewController()) : hideLoaderForController(getCurrentVC() ?? UIViewController())
            }
        }
        // bind error message
        viewModel.onShowError = { message in
            print(message)
        }
        viewModel.albumsData.bind { [weak self] albums in
            self?.title = TITLES.PROFILE.rawValue
            self?.tableView.reloadData()
        }
    }
}
