//
//  PhotosVC.swift
//  ProfileTask
//
//  Created by Ahmed Abuelmagd on 26/03/2021.
//

import UIKit

class PhotosVC: UIViewController {
    
    //MARK: - Variables & Constants
    var albumTitle: String?
    var albumId: Int?
    let viewModel = PhotosVM()
    private var collectionView: UICollectionView?
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        viewModel.albumId = albumId ?? 0
        viewModel.getPhotos()
        initCV()
        title = albumTitle
    }
    func initCV() {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical
    layout.minimumInteritemSpacing = 0
    layout.minimumLineSpacing = 0
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    guard let collectionView = collectionView else { return}
    collectionView.registerCVNib(cell: PhotosCVCell.self)
        collectionView.register(UINib(nibName: CELL_IDENTIFIEERS.PHOTOS_HEADER.rawValue, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CELL_IDENTIFIEERS.PHOTOS_HEADER.rawValue)
    collectionView.showsHorizontalScrollIndicator = false
    collectionView.showsVerticalScrollIndicator = false
    collectionView.delegate = self
    collectionView.dataSource = self
    view.addSubview(collectionView)
    collectionView.frame = view.bounds
    collectionView.backgroundColor = UIColor.white
    }

}
extension PhotosVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CELL_IDENTIFIEERS.PHOTOS_HEADER.rawValue, for: indexPath) as! PhotosCVHeaderCell
        cell.searchDelegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.width * 0.15)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.photosData.value?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCV(index: indexPath) as PhotosCVCell
        if let data = viewModel.photosData.value?[indexPath.row]{
            cell.initCell(cellData: data)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ImageViewerVC.create()
        vc.photo = viewModel.photosData.value?[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}
extension PhotosVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let colomnNumber: CGFloat = 3.0
        let collectionViewWidth = collectionView.bounds.width
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let spaceBetweenCells = flowLayout.minimumLineSpacing
        let adjustedWidth = collectionViewWidth - spaceBetweenCells
        let width = adjustedWidth / colomnNumber
        return CGSize(width: width, height: width)
    }
}
extension PhotosVC{
    /// bind view model
    func bindViewModel(){
        // bind loading
        viewModel.showLoading.bind { [weak self] visible in
            if self != nil {
                visible ? showLoaderForController(getCurrentVC() ?? UIViewController()) : hideLoaderForController(getCurrentVC() ?? UIViewController())
            }
        }
        // bind error message
        viewModel.onShowError = { message in
            print(message)
        }
        viewModel.photosData.bind { [weak self] photos in
            self?.collectionView?.reloadData()
        }
    }
}
extension PhotosVC: SearchDeleget {
    func search(data: String) {
        if data == "" {
            viewModel.albumId = albumId ?? 0
            viewModel.getPhotos()
        } else {
            viewModel.filterImages(searchData: data)
        }
    }
}
